package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;
    public ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return fridge.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridge.size() < max_size) {
            fridge.add(item);
            return true;
        }
        else return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        }
        else throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for (int i = 0; i < fridge.size(); i++) {
            if (fridge.get(i).hasExpired()) {
                expiredFood.add(fridge.get(i));
                takeOut(fridge.get(i));
                i --;
            }
        }
        return expiredFood;
    }
}
